package org.myorg;

import java.io.IOException;
import java.util.regex.Pattern;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;

import org.apache.log4j.Logger;

import org.apache.hadoop.io.NullWritable;
import java.util.Random;

public class RequestCount extends Configured implements Tool {

  private static final Logger LOG = Logger.getLogger(RequestCount.class);

  public static void main(String[] args) throws Exception {
    int res = ToolRunner.run(new RequestCount(), args);
    System.exit(res);
  }

  public int run(String[] args) throws Exception {
    Job job = Job.getInstance(getConf(), "requestcount");
    job.setJarByClass(this.getClass());
    // Use TextInputFormat, the default unless job.setInputFormatClass is used
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    job.setMapperClass(Map.class);
    job.setReducerClass(Reduce.class);
    job.setOutputKeyClass(Text.class);
    //job.setOutputValueClass(IntWritable.class); // should be NullWritable? no, see below;
    job.setOutputValueClass(Text.class); // sets map output? reduce output? both?
    return job.waitForCompletion(true) ? 0 : 1;
  }

  public static class Map extends Mapper<LongWritable, Text, Text, Text> {
    //private final static IntWritable one = new IntWritable(1);
    //private Text word = new Text();
    //private long numRecords = 0;    
    private static final Pattern WORD_BOUNDARY = Pattern.compile("\\s+");

    // input: en Lucille_Ash 1 27543
    // output: "en   Lucille_Ash"   "27543   1"
    public void map(LongWritable offset, Text lineText, Context context)
        throws IOException, InterruptedException {
      String line = lineText.toString();
      //Text currentWord = new Text();
      String[] fields = WORD_BOUNDARY.split(line);
      if(fields.length == 4){
        //String outKey = fields[0] + "\t" + fields[1] + "\t" + fields[3];
        //int outVal = Integer.parseInt(fields[2]);
        //context.write(new Text(outKey), new IntWritable(outVal));
        String outKey = fields[0] + "\t" + fields[1];
        String outVal = fields[2] + "\t" + fields[3];
        
        if(
            !fields[1].contains("%")
            && Character.isLetter(fields[1].charAt(0))
            && !fields[0].contains(".")
            //&& isInteger(fields[2])
            //&& isInteger(fields[3])
            && fields[2].matches("^\\d*$")
            && fields[3].matches("^\\d*$")
        ){
            context.write(new Text(outKey), new Text(outVal));
        }
      }
    }
  }

  // input:  "en   Lucille_Ash"   27543   1
  //         "en   Lucille_Ash"   27543   3
  // output: "en   Lucille_Ash   4   27543"   NULL
  //public static class Reduce extends Reducer<Text, IntWritable, Text, IntWritable> {
  //public static class Reduce extends Reducer<Text, IntWritable, Text, NullWritable> {

  //public static class Reduce extends Reducer<Text, Text, Text, NullWritable> {
  public static class Reduce extends Reducer<Text, Text, Text, Text> {
    @Override
    //public void reduce(Text inKey, Iterable<IntWritable> inVals, Context context)
    public void reduce(Text inKey, Iterable<Text> inVals, Context context)
        throws IOException, InterruptedException {
      //int sum = 0;
      double sumRequests = 0;
      double sumBytes = 0;
      //for (IntWritable count : inVals) {
      for (Text counts : inVals) {
        String lineVals = counts.toString();
        String[] twoVals = lineVals.split("\t");
        //sum += count.get();
        sumRequests += Double.parseDouble(twoVals[0]);
        sumBytes += Double.parseDouble(twoVals[1]);
      }
      int avgBytes = (int)(sumBytes / sumRequests);
      String lineKey = inKey.toString();
      String[] fields = lineKey.split("\t");
      // ### randomize ###
      //Random r = new Random();
      //sumRequests = (double)r.nextInt(10000);
      // ### end randomize ###
      String outKey = fields[0] + "\t" + fields[1] + "\t" + 
        (int)sumRequests + "\t" + avgBytes;
      //NullWritable outVal = NullWritable.get();
      //context.write(new Text(outKey), null);
      //if((int)sumRequests != 1){

      // BY ITEM NAME:
      //if(!fields[1].contains("%") && Character.isLetter(fields[1].charAt(0))){

      // BY REQ COUNT:
      //if((int)sumRequests >= 100){

      // BY FILE SIZE:
      //if(true){  // untested

      // ALL:
      if(
        //(int)sumRequests >= 100
        true
      ){

        context.write(new Text(outKey), null);
      }
      //context.write(new Text(outKey), outVal);
      //context.write(inKey, new IntWritable(sum));
    }
  }

  public static boolean isInteger(String str) {
      if (str == null) {
          return false;
      }
      int length = str.length();
      if (length == 0) {
          return false;
      }
      int i = 0;
      if (str.charAt(0) == '-') {
          if (length == 1) {
              return false;
          }
          i = 1;
      }
      for (; i < length; i++) {
          char c = str.charAt(i);
          if (c <= '/' || c >= ':') {
              return false;
          }
      }
      return true;
  }

}

