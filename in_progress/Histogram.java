package org.myorg;

import java.io.IOException;
import java.util.regex.Pattern;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;

import org.apache.log4j.Logger;

import org.apache.hadoop.io.NullWritable;
import java.util.Random;

public class Histogram extends Configured implements Tool {

  private static final Logger LOG = Logger.getLogger(Histogram.class);

  public static void main(String[] args) throws Exception {
    int res = ToolRunner.run(new Histogram(), args);
    System.exit(res);
  }

  public int run(String[] args) throws Exception {
    Job job = Job.getInstance(getConf(), "histogram");
    job.setJarByClass(this.getClass());
    // Use TextInputFormat, the default unless job.setInputFormatClass is used
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    job.setMapperClass(Map.class);
    job.setReducerClass(Reduce.class);
    job.setOutputKeyClass(IntWritable.class);
    job.setOutputValueClass(IntWritable.class);
    return job.waitForCompletion(true) ? 0 : 1;
  }

  public static class Map extends Mapper<LongWritable, Text, 
    IntWritable, IntWritable> {

    // input:   en   Lucille_Ash   1   27543
    // output:  27   1
    public void map(LongWritable offset, Text lineText, Context context)
        throws IOException, InterruptedException {
      String line = lineText.toString();
      String[] fields = line.split("\t");
        int outKey = Integer.parseInt(fields[3])/1000;
        int outVal = 1;
        context.write(new IntWritable(outKey), new IntWritable(outVal));
    }
  }

  // input:     27   1
  //            28   1
  //            27   1
  //
  // output:    27   2
  //            28   1
  public static class Reduce 
    extends Reducer<IntWritable, IntWritable, IntWritable, IntWritable> {

    @Override
    public void reduce(IntWritable inKey, Iterable<IntWritable> inVals, 
        Context context)
        throws IOException, InterruptedException {
      int sum = 0;
      for (IntWritable count : inVals) {
        sum += count.get();
      }
      context.write(inKey, new IntWritable(sum));
    }
  }
}

