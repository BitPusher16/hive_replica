package org.myorg;
import java.io.IOException;
import java.util.*;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

/**********
 * There is a lot going on in this MapReduce job. Focus on 3 things:
 *
 * 1) We have 2 inputs instead of just one. See line with comment "2nd input."
 *
 * 2) Our input files are already in a key-value format that MapReduce
 *    understands, and it does not need to be modified. We have two options:
 *    - write a Mapper class that simply reads each line of input as plain text
 *      and prints out unchanged key value pairs; or,
 *    - use an identity Mapper (by commenting out setMapperClass() )
 *      and inform MapReduce that our input is already in Key-Value format
 *      (see line with setInputFormat() );
 *    We include code for both approaches. 2nd approach is active.
 *
 * 3) We are doing a join with double nested loops. This is the hardest part
 *    of the lab, conceptually. Read the code carefully.
 */

public class MR3 {

  /*
  public static class Map 
    extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> 
  {
    private Text value_record = new Text();
    private Text uid = new Text();

    public void map(LongWritable key, Text value, 
      OutputCollector<Text, Text> output, Reporter reporter) throws IOException 
    {
      String line = value.toString();
      String[] tab_delim = line.split("\\t");

      value_record.set(tab_delim[1]);
      uid.set(tab_delim[0]);
      output.collect(uid, value_record);
    }
  }
  */

  public static class Reduce 
    extends MapReduceBase implements Reducer<Text, Text, Text, Text> 
  {
    public void reduce(Text key, Iterator<Text> values, 
      OutputCollector<Text, Text> output, Reporter reporter) throws IOException 
    {

      String ValueRecord = new String();
      String MovieID = new String();
      String UserID = new String();
      List<String> outer = new ArrayList<String>();
      List<String> inner = new ArrayList<String>();
      String[]elements = new String[2];

      // cache inner and outer tables;
      while(values.hasNext()){
        String line = values.next().toString();
        elements = line.toString().split("::");
        if(elements[0].equals("UsersTable")){
          outer.add(line);
        }
        else if(elements[0].equals("RatingsTable")){
          inner.add(line);
        }
      }

      // since reducers only get k-v pairs with matching keys,
      // we already know that all entries in our two tables are valid joins;
      // use double nested loops to print out all results;
      int outer_size = outer.size();
      int inner_size = inner.size();
      for(int i = 0; i < outer_size; i++){
        for(int j = 0; j < inner_size; j++){
          elements = inner.get(j).split("::");
          MovieID = elements[1];
          ValueRecord = "UsersRatingsTable";
          output.collect(new Text(MovieID), new Text(ValueRecord));
        }
      }
    }
  }

  public static void main(String[] args) throws Exception {
    JobConf conf = new JobConf(MR3.class);
    conf.setJobName("UsersRatings");

    FileInputFormat.setInputPaths(conf, new Path(args[0]));
    FileInputFormat.addInputPath(conf, new Path(args[1])); // 2nd input;
    FileOutputFormat.setOutputPath(conf, new Path(args[2]));

    //conf.setInputFormat(TextInputFormat.class);
    conf.setInputFormat(KeyValueTextInputFormat.class);
    conf.setOutputFormat(TextOutputFormat.class);

    conf.setOutputKeyClass(Text.class);
    conf.setOutputValueClass(Text.class);

    //conf.setMapperClass(Map.class);
    //conf.setCombinerClass(Reduce.class);
    conf.setReducerClass(Reduce.class);

    JobClient.runJob(conf);
  }
}




