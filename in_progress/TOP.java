package org.myorg;
// taken from https://gist.github.com/asimjalis/e5627dc2ff2b23dac70b

import java.io.*;

import org.apache.hadoop.conf.*;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.*;
import org.apache.hadoop.mapreduce.lib.output.*;
import org.apache.hadoop.mapreduce.lib.partition.*;
import org.apache.hadoop.mapreduce.lib.reduce.*;
import org.apache.hadoop.util.*;

import org.apache.hadoop.io.IntWritable;

/**
 * Demonstrates how to use Total Order Partitioner on Word Count.
 */

//public static class WordCount extends Configured implements Tool {
public class TOP extends Configured implements Tool {

    private final static int REDUCE_TASKS = 8;

    public static void main(String[] args) throws Exception {
        //int exitCode = ToolRunner.run(new WordCount(), args);
        int exitCode = ToolRunner.run(new TOP(), args);
        System.exit(exitCode);
    }

    @Override @SuppressWarnings({ "unchecked", "rawtypes" })
    public int run(String[] args) throws Exception {
      // Check arguments.
      if (args.length != 2) {
        String usage =
          "Usage: " +
          //"hadoop jar TOP$WordCount " +
          "hadoop jar TOP " +
          "<input dir> <output dir>\n";
        System.out.printf(usage);
        System.exit(-1);
      }

      String jobName = "TotalOrder";
      String mapJobName = jobName + "-Map";
      String reduceJobName = jobName + "-Reduce";

      // Get user args.
      String inputDir = args[0];
      String outputDir = args[1];

      // Define input path and output path.
      Path mapInputPath = new Path(inputDir);
      Path mapOutputPath = new Path(outputDir + "-inter");
      Path reduceOutputPath = new Path(outputDir);

      // Define partition file path.
      Path partitionPath = new Path(outputDir + "-part.lst");

      //======
      // Job #1 (mapJob)
      //======

      // Configure map-only job for sampling.
      Job mapJob = new Job(getConf());
      mapJob.setJobName(mapJobName);
      //mapJob.setJarByClass(WordCount.class);
      mapJob.setJarByClass(TOP.class);
      mapJob.setMapperClass(WordMapper.class);            // defined below;
      mapJob.setNumReduceTasks(0);
      //mapJob.setOutputKeyClass(Text.class);             // mapper output?
      //mapJob.setOutputValueClass(IntWritable.class);    // mapper output?

      // BY ITEM NAME:
      /*
      mapJob.setOutputKeyClass(Text.class);
      mapJob.setOutputValueClass(Text.class);
      */

      // BY REQ COUNT, FILE SIZE:
      mapJob.setOutputKeyClass(IntWritable.class);  // !!
      mapJob.setOutputValueClass(Text.class);       // !!

      TextInputFormat.setInputPaths(mapJob, mapInputPath);
      // Set the output format to a sequence file.
        // Is this output format required for total partition?
        // Yes: see javadoc for TotalOrderPartitioner:
        // "Set the path to the SequenceFile storing the sorted partn keyset."
      mapJob.setOutputFormatClass(SequenceFileOutputFormat.class);
      SequenceFileOutputFormat.setOutputPath(mapJob, mapOutputPath);

      // Submit the map-only job.
      int exitCode = mapJob.waitForCompletion(true) ? 0 : 1;
      if (exitCode != 0) { return exitCode; }

      //=======
      // Job #2 (reduceJob)
      //=======

      // Set up the second job, the reduce-only.
      Job reduceJob = new Job(getConf());
      reduceJob.setJobName(reduceJobName);
      //reduceJob.setJarByClass(WordCount.class);
      reduceJob.setJarByClass(TOP.class);

      // Set the input to the previous job's output.
      reduceJob.setInputFormatClass(SequenceFileInputFormat.class);
      SequenceFileInputFormat.setInputPaths(reduceJob, mapOutputPath);

      // Set the output path to the final output path.
      TextOutputFormat.setOutputPath(reduceJob, reduceOutputPath);

      // Use identity mapper for key/value pairs in SequenceFile.
        // no mapper specified; implied by omission?
      
      /*
      reduceJob.setReducerClass(IntSumReducer.class);
      reduceJob.setMapOutputKeyClass(Text.class);
      reduceJob.setMapOutputValueClass(IntWritable.class);
      reduceJob.setOutputKeyClass(Text.class);
      reduceJob.setOutputValueClass(IntWritable.class);
      */

      // BY ITEM NAME:
      /*
      reduceJob.setMapOutputKeyClass(Text.class);
      reduceJob.setMapOutputValueClass(Text.class);
      reduceJob.setOutputKeyClass(Text.class);
      reduceJob.setOutputValueClass(Text.class);
      */

      // BY REQ COUNT, FILE SIZE:
      reduceJob.setMapOutputKeyClass(IntWritable.class);
      reduceJob.setMapOutputValueClass(Text.class);
      reduceJob.setOutputKeyClass(IntWritable.class);
      reduceJob.setOutputValueClass(Text.class);


      reduceJob.setSortComparatorClass(SortKeyComparator.class); // !!
      reduceJob.setReducerClass(WordReducer.class);      // defined below;

      reduceJob.setNumReduceTasks(REDUCE_TASKS);

      // Use Total Order Partitioner.
      reduceJob.setPartitionerClass(TotalOrderPartitioner.class);

      // Generate partition file from map-only job's output.
      TotalOrderPartitioner.setPartitionFile(
          reduceJob.getConfiguration(), partitionPath);
      InputSampler.writePartitionFile(reduceJob, new InputSampler.RandomSampler(
          1, 10000));

      // Submit the reduce job.
      return reduceJob.waitForCompletion(true) ? 0 : 2;
    }

    public static class WordMapper extends
        //Mapper<LongWritable, Text, Text, IntWritable> {

        // BY ITEM NAME:
        //Mapper<LongWritable, Text, Text, Text> {

        // BY REQ COUNT, FILE SIZE:
        Mapper<LongWritable, Text, IntWritable, Text> {

        @Override
        public void map(LongWritable key, Text value, Context context)
            throws IOException, InterruptedException {
            String line = value.toString();
            String[] fields = line.split("\t");

            // BY ITEM NAME:
            /*
            context.write(
                new Text(fields[1]),
                //null
                new Text(fields[0]+"\t"+fields[2]+"\t"+fields[3])
            );
            */

            // BY REQUEST COUNT:
            context.write(
                new IntWritable(Integer.parseInt(fields[2])),
                new Text(fields[0]+"\t"+fields[1]+"\t"+fields[3])
            );

            // BY FILE SIZE:
            /*
            context.write(
                new IntWritable(Integer.parseInt(fields[3])),
                new Text(fields[0]+"\t"+fields[1]+"\t"+fields[2])
            );
            */

            /*
            for (String word : line.split("\\W+")) {
              if (word.length() == 0) { continue; }
              context.write(new Text(word), new IntWritable(1));
            }
            */
        }
    }

    public static class WordReducer extends
        //Reducer<IntWritable, Text, IntWritable, Text> {

        // BY ITEM NAME:
        /*
        Reducer<Text, Text, Text, Text> {
        @Override
        public void reduce(Text key, Iterable<Text> value, 
            Context context)
        */

        // BY REQ COUNT, FILE SIZE:
        Reducer<IntWritable, Text, Text, Text> {
        @Override
        public void reduce(IntWritable key, Iterable<Text> value, 
            Context context)

        throws IOException, InterruptedException {
            for(Text val : value){
                String line = val.toString();
                String[] fields = line.split("\t");
                context.write(
                    /*
                    key, 
                    val
                    */

                    // BY ITEM NAME:
                    /*
                    new Text(fields[0]+"\t"+key.toString()+"\t"+
                        fields[1]+"\t"+fields[2]
                    ),
                    null
                    */

                    // BY REQUEST COUNT:
                    new Text(fields[0]+"\t"+fields[1]+"\t"+
                        key.toString()+"\t"+fields[2]
                    ),
                    null

                    // BY FILE SIZE:
                    /*
                    new Text(fields[0]+"\t"+fields[1]+"\t"+
                        fields[2]+"\t"+key.toString()
                    ),
                    null
                    */
                );
            }
        }
    }

    public static class SortKeyComparator extends WritableComparator {
       
      // BY ITEM NAME:

      /*
      protected SortKeyComparator() {
          super(Text.class, true);
      }

      @Override
      public int compare(WritableComparable a, WritableComparable b) {
          Text o1 = (Text) a;
          Text o2 = (Text) b;
          return o1.toString().compareTo(o2.toString());
      }
      */

      // BY REQ COUNT, FILE SIZE:

      /**
       * Compares in the descending order of the keys.
       */
      protected SortKeyComparator() {
          //super(LongWritable.class, true);
          super(IntWritable.class, true);
      }

      @Override
      public int compare(WritableComparable a, WritableComparable b) {
          //LongWritable o1 = (LongWritable) a;
          //LongWritable o2 = (LongWritable) b;
          IntWritable o1 = (IntWritable) a;
          IntWritable o2 = (IntWritable) b;
          if(o1.get() < o2.get()) {
              return 1;
          }else if(o1.get() > o2.get()) {
              return -1;
          }else {
              return 0;
          }
      }
    }

} // added by user;
