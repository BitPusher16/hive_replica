package jb;

import java.io.IOException;
import java.util.*;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

public class Jobs {

  public static class SelectMapper 
    extends MapReduceBase implements Mapper<LongWritable, Text, NullWritable, Text> 
  {

    //private Text outKey = new Text();
    //private Text outVal = new Text();
    private String args;

    // get configuration passed from master;
    // expect predicate to have format: "colA=valA,colB<valB,colC>valC"
    // for now, columns and values must be integers:
    // "0=20,1>5"
    @Override
    public void configure(JobConf jconf){ args = jconf.get("args"); }

    public boolean checkPredicate(String line, String predicate){
        //if(1 == 1) return true;

        String[] elements = line.split("::");
        String[] predAtoms = predicate.split(",");
        for(int i = 0; i < predAtoms.length; i++){
            if(predAtoms[i].contains("=")){
                String[] atomElts = predAtoms[i].split("=");
                int colVal = Integer.parseInt(
                    elements[Integer.parseInt(atomElts[0])]
                );
                int predVal = Integer.parseInt(atomElts[1]);
                if(!(colVal == predVal)){ return false; }
            }
            else if(predAtoms[i].contains("<")){
                String[] atomElts = predAtoms[i].split("<");
                int colVal = Integer.parseInt(
                    elements[Integer.parseInt(atomElts[0])]
                );
                int predVal = Integer.parseInt(atomElts[1]);
                if(!(colVal < predVal)){ return false; }
            }
            else if(predAtoms[i].contains(">")){
                String[] atomElts = predAtoms[i].split(">");
                int colVal = Integer.parseInt(
                    elements[Integer.parseInt(atomElts[0])]
                );
                int predVal = Integer.parseInt(atomElts[1]);
                if(!(colVal > predVal)){ return false; }
            }
        }
        return true;
    }

    public void map(LongWritable key, Text value, 
      OutputCollector<NullWritable, Text> output, Reporter reporter) throws IOException 
    {
      String line = value.toString();
      //String[] elements = line.split("::");

      // unpack arguments;
      // only support one argument for now;
      //int col = Integer.parseInt(args.split("=")[0]);
      //String colVal = args.split("=")[1];

      //if(elements[col].equals(colVal)){
      if(checkPredicate(line, args)){
      //if(true){
          output.collect(NullWritable.get(), value);
          //output.collect(NullWritable.get(), new Text(args + "|" + Integer.toString(col) + "|" + colVal));
      }
    } // end map()
  } // end SelectMapper

  public static class ProjectMapper extends MapReduceBase 
    implements Mapper<LongWritable, Text, NullWritable, Text> 
  {

    //private Text outKey = new Text();
    //private Text outVal = new Text();
    private String args;

    // get configuration passed from master;
    // expect args to have format: "0,3,1"
    @Override
    public void configure(JobConf jconf){ args = jconf.get("args"); }

    public void map(LongWritable key, Text value, 
      OutputCollector<NullWritable, Text> output, Reporter reporter) throws IOException 
    {
      String line = value.toString();
      String[] elements = line.split("::");

      // unpack arguments;
      // only support one argument for now;
      //int col = Integer.parseInt(args.split("=")[0]);

      // select columns;
      String[] cols = args.split(",");
      String projectedStr = "";
      for(int i = 0; i < cols.length; i++){
        //projectedStr.concat(elements[Integer.parseInt(cols[i])]);
        projectedStr += elements[Integer.parseInt(cols[i])];
        if(i < cols.length - 1){
            projectedStr += "::";
        }
      }

      output.collect(NullWritable.get(), new Text(projectedStr));
    } // end map()
  } // end ProjectMapper

  // NOTE: currently only implements COUNT;
  public static class GroupByMapper extends MapReduceBase 
    //implements Mapper<LongWritable, Text, NullWritable, Text> 
    implements Mapper<LongWritable, Text, Text, Text> 
  {
    private String args;

    // get configuration passed from master;
    // expect args to have format: "2,1" (indexes of GroupBy columns)
    @Override
    public void configure(JobConf jconf){ args = jconf.get("args"); }

    public void map(LongWritable key, Text value, 
      //OutputCollector<NullWritable, Text> output, Reporter reporter) throws IOException 
      OutputCollector<Text, Text> output, Reporter reporter) throws IOException 
    {
      String line = value.toString();
      String[] elements = line.split("::");

      // select columns;
      String[] cols = args.split(",");
      String outKey = "";
      for(int i = 0; i < cols.length; i++){
        outKey += elements[Integer.parseInt(cols[i])];
        if(i < cols.length - 1){
            outKey += "::";
        }
      }

      output.collect(new Text(outKey), new Text("1"));
    } // end map()
  } // end GroupByMapper

  public static class GroupByReducer 
    //extends MapReduceBase implements Reducer<Text, Text, Text, Text> 
    extends MapReduceBase implements Reducer<Text, Text, NullWritable, Text> 
  {
    private Text outKey = new Text();
    private Text outValue = new Text();
      
    public void reduce(Text key, Iterator<Text> values, 
      //OutputCollector<Text, Text> output, Reporter reporter) throws IOException 
      OutputCollector<NullWritable, Text> output, Reporter reporter) throws IOException 
    {
      int count = 0;
      while(values.hasNext()){
        count += Integer.parseInt(values.next().toString());
      }
      outValue.set(key.toString() + "::" + Integer.toString(count));
      output.collect(NullWritable.get(), outValue);
    }
  } // end GroupByReducer
} // end Jobs


