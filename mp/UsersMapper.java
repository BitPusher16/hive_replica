package mp;
import java.io.IOException;
import java.util.*;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

public class UsersMapper {

  public static class Map 
    // mapper input key is always type LongWritable;
    // by looking at our input file, we know mapper value input is type Text;
    // we want mapper output key to be type Text;
    // we want mapper output value to be type Text;
    extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> 
  {

    private Text value_record = new Text();
    private Text user_id = new Text();
    private String occupation;

    @Override
    public void configure(JobConf jconf){
        //super.configure(jconf);
        occupation = jconf.get("occupation");
    }

    //Configuration conf = context.getConfiguration();
    //Configuration conf = JobContext.getConfiguration();
    //occupation = conf.get("occupation");

    public void map(LongWritable key, Text value, 
      OutputCollector<Text, Text> output, Reporter reporter) throws IOException 
    {
      //Configuration conf = context.getConfiguration();
      //occupation = conf.get("occupation");


      String line = value.toString();
      String[] elements = line.split("::");

      value_record.set("UsersTable");
      user_id.set(elements[0]);
      //if(elements[3].equals("12")){
      if(elements[3].equals(occupation)){
          output.collect(user_id, value_record);
      }
    } // end map()
  } // end Map

  public static class Histogram 
    // mapper input key is always type LongWritable;
    // by looking at our input file, we know mapper value input is type Text;
    // we want mapper output key to be type Text;
    // we want mapper output value to be type Text;
    extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> 
  {

    private Text outKey = new Text();
    private Text outVal = new Text();

    public void map(LongWritable key, Text value, 
      OutputCollector<Text, Text> output, Reporter reporter) throws IOException 
    {
      String line = value.toString();
      String[] elements = line.split("\t");

      outKey.set(
        Integer.toString(Integer.parseInt(elements[0]) / 1000)
      );
      outVal.set("1");
      output.collect(outKey, outVal);
    } // end map()
  } // end Histogram

  public static class HistogramReduce 
    //extends MapReduceBase implements Reducer<Text, Text, Text, Text> 
    extends MapReduceBase implements Reducer<Text, Text, NullWritable, Text> 
  {
    public void reduce(Text key, Iterator<Text> values, 
      //OutputCollector<Text, Text> output, Reporter reporter) throws IOException 
      OutputCollector<NullWritable, Text> output, Reporter reporter) throws IOException 
    {
      int sum = 0;
      while(values.hasNext()){
        String inVal = values.next().toString();
        sum += Integer.parseInt(inVal);
      }
      Text outVal = new Text(Integer.toString(sum));
      //output.collect(key, outVal);
      output.collect(NullWritable.get(), outVal);

    }
  } // end HistogramReduce
} // end UsersMapper


