package mrq;

public class RelationalOperator{
    public String op;       // relational operation to perform;
    public String src;      // source file in HDFS;
    public String dest;     // destination file in HDFS;
    public String args;     // operator arguments; can be predicates for SELECT,
                            // join columns for JOIN, etc.

    public RelationalOperator(String argOp, String argSrc,
        String argDest, String argArgs)
    {
        op = argOp;
        src = argSrc;
        dest = argDest;
        args = argArgs;
    }

    public void dumpOperator(){
        //System.out.println("op: " + op);
        //System.out.println("src: " + src);
        //System.out.println("dest: " + dest);
        //System.out.println("args: " + args);
        System.out.println("op:" + op + " src:" + src + " dest:" + dest + " args:" + args);
    }

    public String getOp(){ return op; }
    public String getSrc(){ return src; }
    public String getDest(){ return dest; }
    public String getArgs(){ return args; }
}
