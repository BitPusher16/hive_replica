package mrq; // mapreduce query;

// classpath provided by Cloudera tutorial doesn't work:
// $ HP=/opt/cloudera/parcels/CDH/lib/hadoop/*:/opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/* # ?

// use this one instead:
// $ HP=$(hadoop classpath)

// NOTE: $(hadoop classpath) and the path given in the Cloudera WordCount
// tutorial are pointed to the same place;
// do ls -l in the directory /opt/cloudera/parcels;

// to view hadoop classpath:
// $ hadoop classpath | tr : '\n'

// to compile/jar/run:
// from parent of jb directory, mrq directory:
// $ javac -cp $HP:. jb/Jobs.java
// $ jar cvf jb.jar jb
// $ javac -cp $HP:. mrq/Driver.java
// $ java -cp $HP:. mrq/Driver

// working queries:
// select /user/rodgers9/mlens/input/ratings.dat /user/rodgers9/mlens/out3 0=3,2<4
// project /user/rodgers9/mlens/input/users.dat /user/rodgers9/mlens/out4 4,1
// groupby /user/rodgers9/mlens/input/ratings.dat /user/rodgers9/mlens/out5 1,2

// select /user/rodgers9/mlens/input/ratings_11G.dat /user/rodgers9/mlens/out6 0=3,2<4

import java.io.*; // BufferedReader, InputStreamReader;

//import mrq.*;

public class Driver{
    public static void main(String[] args){
        System.out.println("type quit to stop program;");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s = "";
        while(!s.equals("quit")){
            try{
                System.out.print("mrq> ");
                s = br.readLine();
                if(s.equals("quit")){ continue; }
                QueryPlan qp = new QueryPlan();
                qp.parseQuery(s);
                qp.dumpQueryPlan();

                System.out.println("driver launching query;");
                qp.executeQuery();
                System.out.println("driver finished query;");
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
    }
}
