package mrq;

import java.io.IOException;
import java.util.*;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

//import java.io.*;       // String.toLowerCase()
import java.util.List;     // List<>, ArrayList<>();
import java.util.ArrayList;

import org.apache.hadoop.fs.FileSystem;
//import mp.*;    // this imports ./mp or ./mp.jar?

public class QueryPlan{

    public List<RelationalOperator> lro; 
        // review difference between interface, class;
        // http://stackoverflow.com/questions/9988211/listobject-and-list
        // also, is it standard practice to initialize class vars 
        // in constructor?

    public QueryPlan(){
        lro = new ArrayList<RelationalOperator>();
    }

    public void dumpQueryPlan(){
        System.out.println("num ops: " + lro.size());
        for(int i = 0; i < lro.size(); i++){
            lro.get(i).dumpOperator();
        }
    }

    public void parseQuery(String s){
        String[] lines = s.split(";");
        System.out.println("num lines: " + lines.length);
        for(int i = 0; i < lines.length; i++){
            //String[] tokens = s.split("\\s+");
            //System.out.println(lines[i]);
            String[] tokens = lines[i].trim().split("\\s+");
            if(tokens[0].equals("select")){
                RelationalOperator ro = new RelationalOperator(
                    tokens[0], tokens[1], tokens[2], tokens[3]
                );
                lro.add(ro);
            }
            else if(tokens[0].equals("project")){
                RelationalOperator ro = new RelationalOperator(
                    tokens[0], tokens[1], tokens[2], tokens[3]
                );
                lro.add(ro);
            }
            else if(tokens[0].equals("groupby")){
                RelationalOperator ro = new RelationalOperator(
                    tokens[0], tokens[1], tokens[2], tokens[3]
                );
                lro.add(ro);
            }
            else if(tokens[0].equals("def1")){   // default 1
                RelationalOperator ro = new RelationalOperator(
                    "select",
                    "/user/rodgers9/mlens/input/users.dat",
                    "/user/rodgers9/mlens/tmpout5",
                    //"2=25,3=12" // column=value;
                    "2=25" // column=value;
                );
                lro.add(ro);
            }
            else if(tokens[0].equals("def2")){   // default 2
                RelationalOperator ro = new RelationalOperator(
                    "select",
                    "/user/rodgers9/mlens/input/users.dat",
                    "/user/rodgers9/mlens/tmpout5",
                    "3=25"
                );
                lro.add(ro);
                ro = new RelationalOperator(
                    "project",
                    "/user/rodgers9/mlens/input/tmpout5",
                    "/user/rodgers9/mlens/tmpout6",
                    "0,1"
                );
                lro.add(ro);
            }
        }
    } // end parseQuery();

    public void executeQuery() throws Exception{
        for(int i = 0; i < lro.size(); i++){
            RelationalOperator ro = lro.get(i);

            Configuration conf = new Configuration();

            // are these configurations already set elsewhere by the system?
            //conf.set("yarn.resourcemanager.address", "openstack-vm-11-103.rcac.purdue.edu:8032");
            //conf.set("mapreduce.framework.name", "yarn");
            //conf.set("fs.defaultFS", "hdfs://openstack-vm-11-103.rcac.purdue.edu:8020");

            // set args to pass to mapper/reducer;
            conf.set("args",ro.getArgs());

            JobConf jconf = new JobConf(conf);  // this must come AFTER setting configs;
            
            // choose which job to run;
            jconf.setJar("jb.jar");
            if(ro.getOp().equals("select")){
                // choose select class;
                jconf.setMapperClass(jb.Jobs.SelectMapper.class);
                //jconf.setReducerClass(mp.UsersMapper.HistogramReduce.class);
                jconf.setNumReduceTasks(0);
            }
            else if(ro.getOp().equals("project")){
                // choose project class;
                jconf.setMapperClass(jb.Jobs.ProjectMapper.class);
                jconf.setNumReduceTasks(0);
            }
            else if(ro.getOp().equals("groupby")){
                jconf.setMapperClass(jb.Jobs.GroupByMapper.class);
                jconf.setReducerClass(jb.Jobs.GroupByReducer.class);
            }

            jconf.setOutputKeyClass(Text.class);
            jconf.setOutputValueClass(Text.class);

            jconf.setInputFormat(TextInputFormat.class);
            jconf.setOutputFormat(TextOutputFormat.class);

            FileInputFormat.setInputPaths(jconf, new Path(ro.getSrc()));
            FileSystem fs = FileSystem.get(jconf);
            Path out = new Path(ro.getDest());
            fs.delete(out, true);
            FileOutputFormat.setOutputPath(jconf, out);
            
            JobClient.runJob(jconf);

        }
    } // end executeQuery();
} // end QueryPlan;
