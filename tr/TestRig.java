package tr;
import java.io.IOException;
import java.util.*;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

//import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.fs.FileSystem;
import mp.*;    // this imports ./mp or ./mp.jar?
                // I think ./mp.jar, because it works even when ./mp is hidden;
                // or maybe it depends on whether .jar is available; (?)
                // UPDATE: not needed?

// $ javac -cp $CP:./mp.jar TestRig.java
// $ javac -cp $CP:./mp.jar:/home/rodgers9/slf4j-1.7.19/slf4j-api-1.7.19.jar TestRig.java
// $ java -cp $CP:./mp.jar:/home/rodgers9/slf4j-1.7.19/slf4j-api-1.7.19.jar:. TestRig

// $ javac -cp $(hadoop classpath):$CP:./mp.jar:/home/rodgers9/slf4j-1.7.19/slf4j-api-1.7.19.jar TestRig.java
// $ java -cp $(hadoop classpath):$CP:./mp.jar:/home/rodgers9/slf4j-1.7.19/slf4j-api-1.7.19.jar:. TestRig

// $ javac -cp $(hadoop classpath):$CP:./mp.jar TestRig.java
// $ java -cp $(hadoop classpath):$CP:./mp.jar:. TestRig

// $ CP=$HP:$CLDR:.:./mp.jar:./tr

// $ HP=$(hadoop classpath)
// $ javac -cp $HP:. tr/TestRig.java // why can we compile this without mp.jar?
                                    // because this uses the code in ./mp
// $ java -cp $HP:.:./mp.jar tr/TestRig

// $ HP=$(hadoop classpath)
// $ javac -cp $HP:.:./mp.jar tr/TestRig.java
// $ java -cp $HP:.:./mp.jar tr/TestRig

// to prepare mapper jar:
// $ javac -cp $HP mp/UsersMapper.java
// $ jar cvf mp.jar mp
// or, to avoid cruft in mp,
// $ jar cvf mp.jar mp/...? // need to include UsersMapper.class or UsersMapper$Map.class?

public class TestRig {

  public static void main(String[] args) throws Exception {

    /*
    JobConf conf = new JobConf(MR1.class);
    conf.setJobName("Users");

    // which HDFS file should we read?
    // which HDFS file should we write to?
    FileInputFormat.setInputPaths(conf, new Path(args[0]));
    FileOutputFormat.setOutputPath(conf, new Path(args[1]));

    // file input/output format could be one of:
    // SequenceFileInputFormat, TextInputFormat, KeyValueTextInputFormat, etc.
    conf.setInputFormat(TextInputFormat.class);
    conf.setOutputFormat(TextOutputFormat.class);

    conf.setOutputKeyClass(Text.class);
    conf.setOutputValueClass(Text.class);

    conf.setMapperClass(Map.class);
    //conf.setCombinerClass(Reduce.class); // we don't need a combiner;
    //conf.setReducerClass(Reduce.class);  // we don't need a reducer;
    conf.setNumReduceTasks(0); // disable shuffle phase, reduce phase;

    JobClient.runJob(conf);
    */

    // Job 1

    Configuration conf = new Configuration();
    //Job job = new Job(conf); // deprecated?
    //JobConf jconf = new JobConf(conf);
        // declare later?
        // UPDATE: it seems none of the configs I set below were taking effect;

    // QUESTION: These configurations are set automatically?
    // my jobs were working even when I (incorrectly) called new JobConf(conf)
    // before setting the config values;
    conf.set("yarn.resourcemanager.address", "openstack-vm-11-103.rcac.purdue.edu:8032");
    //conf.set("yarn.resourcemanager.address", "openstack-vm-11-103.rcac.purdue.edu:8888");
        // taken from Cloudera Manager -> YARN -> ResourceManager Web UI -> tools -> configuration;
    conf.set("mapreduce.framework.name", "yarn");
    //conf.set("fs.default.name", "hdfs://openstack-vm-11-103.rcac.purdue.edu:8020");
    conf.set("fs.defaultFS", "hdfs://openstack-vm-11-103.rcac.purdue.edu:8020");
    //conf.set("fs.defaultFS", "hdfs://openstack-vm-11-103.rcac.purdue.edu:8888");
    //conf.set("fs.defaultFS", "hdfs://172.18.11.103:8020");
        // taken from Cloudera Manager -> HDFS -> NameNode Web UI;

    // set a parameter to pass to the mapper;
    conf.set("occupation","8");

    JobConf jconf = new JobConf(conf);
    
    jconf.setMapperClass(mp.UsersMapper.Map.class);
    //jconf.setJarByClass(mp.UsersMapper.Map.class);
    jconf.setJar("mp.jar");
    jconf.setNumReduceTasks(0);

    jconf.setOutputKeyClass(Text.class);
    jconf.setOutputValueClass(Text.class);

    jconf.setInputFormat(TextInputFormat.class);
    jconf.setOutputFormat(TextOutputFormat.class);

    FileInputFormat.setInputPaths(jconf, new Path("/user/rodgers9/mlens/input/users.dat"));
    //FileInputFormat.setInputPaths(jconf, new Path("file:///user/rodgers9/mlens/input/users.dat"));
    FileSystem fs = FileSystem.get(jconf);
    Path out = new Path("/user/rodgers9/mlens/tmpout3");
    //Path out = new Path("file:///user/rodgers9/mlens/tmpout3");
    fs.delete(out, true);
    FileOutputFormat.setOutputPath(jconf, out);
    
    jconf.setInputFormat(TextInputFormat.class);
    jconf.setOutputFormat(TextOutputFormat.class);

    //jconf.waitForCompletion(true);
    JobClient.runJob(jconf);
        // runJob waits until job completes before returning;
        // submitJob ...

    // Job 2

    Configuration conf2 = new Configuration();

    conf2.set("yarn.resourcemanager.address", "openstack-vm-11-103.rcac.purdue.edu:8032");
    conf2.set("mapreduce.framework.name", "yarn");
    conf2.set("fs.defaultFS", "hdfs://openstack-vm-11-103.rcac.purdue.edu:8020");

    JobConf jconf2 = new JobConf(conf2);
    
    jconf2.setMapperClass(mp.UsersMapper.Histogram.class);
    jconf2.setReducerClass(mp.UsersMapper.HistogramReduce.class);
    jconf2.setJar("mp.jar");
    //jconf2.setNumReduceTasks(0);

    jconf2.setOutputKeyClass(Text.class);
    jconf2.setOutputValueClass(Text.class);

    jconf2.setInputFormat(TextInputFormat.class);
    jconf2.setOutputFormat(TextOutputFormat.class);

    //FileInputFormat.setInputPaths(jconf2, new Path("/user/rodgers9/mlens/input/users.dat"));
    FileInputFormat.setInputPaths(jconf2, new Path("/user/rodgers9/mlens/tmpout3"));
    FileSystem fs2 = FileSystem.get(jconf2);
    Path out2 = new Path("/user/rodgers9/mlens/tmpout4");
    fs2.delete(out2, true);
    FileOutputFormat.setOutputPath(jconf2, out2);
    
    // duplicate?
    jconf2.setInputFormat(TextInputFormat.class);
    jconf2.setOutputFormat(TextOutputFormat.class);

    JobClient.runJob(jconf2);
  }
}


